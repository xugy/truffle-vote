# 用区块链投票的练习项目
## 一、安装 nodejs, npm
#### 1、先简单安装 nodejs 及 npm, 之后
    sudo npm install -g nrm
    nrm use taobao
    
#### 2、全局安装 n 模块, 方便自由选择 node 版本
    sudo npm install -g n
    sudo n v9

#### 3、在 node v9 环境下再次更新 npm
    sudo npm install -g npm

## 二、安装 ganache-cli。注：ethereum-testrpc 已被 ganache-cli 替代
    sudo npm install -g ganache-cli 

## 三、启动私链
    ganache-cli

## 四、部署合约
    truffle migrate --reset

## 五、安装 Chrome MetaMask 插件
https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=zh-CN

## 六、创建账户
[参照这篇文章的末尾创建账户](https://steemit.com/cn/@lucia3/ethereum-pet-shop)

## 七、启动应用
    npm run dev
启动应用后，会自动打开浏览器窗口，可进行投票，此时若提示投票账户 eth 为 0，可点击投票左侧投票者的地址，以进行 copy，之后继续进行以下步骤

## 八、Ethereum Wallet 钱包连接到私链
    /Applications/Ethereum\ Wallet.app/Contents/MacOS/Ethereum\ Wallet --rpc http://localhost:8545/
1、点击 SEND，to 位置填入刚刚复制的账户地址，金额位置输入一些以太币，比如10个。
2、点击 页面底部 SEND，此时提示输入「出」方的账号密码，由于是 ganache-cli 创建的私链，此时随意输入一些数字即可，稍等片刻，再回到 MetaMask，会发现当前账号已有了一些以太币。

## 九、重新点击投票 ～
